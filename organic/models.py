from django.db import models
from django.contrib import admin
#base usernya django
from django.contrib.auth.models import User
from userapp.models import UserProfile
from django.template import defaultfilters
from decimal import Decimal
import datetime, os
from datetime import datetime as dt
import urllib2
from tinymce import HTMLField

def get_p1_upload(instance, filename):
    return os.path.join('photo_1/', dt.now().date().strftime("%Y/%m/%d"), filename)

def get_p2_upload(instance, filename):
    return os.path.join('photo_2/', dt.now().date().strftime("%Y/%m/%d"), filename)

def get_p3_upload(instance, filename):
    return os.path.join('photo_3/', dt.now().date().strftime("%Y/%m/%d"), filename)

def get_article_upload(instance, filename):
    return os.path.join('photo_article/', dt.now().date().strftime("%Y/%m/%d"), filename)

def get_galery_upload(instance, filename):
    return os.path.join('photo_galery/', dt.now().date().strftime("%Y/%m/%d"), filename)

class Product(models.Model):
    name = models.CharField(max_length=255, unique=True)
    slug = models.CharField(max_length=300, blank=True, null=True, unique=True)
    shortdesc = models.CharField(max_length=300, blank=True, null=True)

    description  = models.TextField(blank=True, null=True, help_text="Deskripsi produk organik blablabla") 
    active = models.BooleanField(default=True)
    pricemarket = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=2)
    price = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=2)
    ratings = models.FloatField(default=0.0)
    like = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    
    toped = models.CharField(max_length=500, blank=True, null=True)
    bukalapak = models.CharField(max_length=500, blank=True, null=True)

    photo1 = models.ImageField('Photo', upload_to=get_p1_upload, blank=True, null=True)
    photo2 = models.ImageField('Photo', upload_to=get_p2_upload, blank=True, null=True)
    photo3 = models.ImageField('Photo', upload_to=get_p3_upload, blank=True, null=True)
    
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name)
        super(Product, self).save(*args, **kwargs)

    def waurl(self):
        text = """Halo, Saya ingin memesan %s
Jumlah: 
Kota:
Alamat:

Terima kasih."""% self.name
        text = urllib2.quote(text)
        phone = "6287781737152"
        return "https://api.whatsapp.com/send?phone=%s&text=%s" % (phone, text)

    def smstext(self):
        text = """Halo, Saya ingin memesan %s
Jumlah: 
Kota:
Alamat:

Terima kasih."""% self.name
        text = urllib2.quote(text)
        phone = "6287781737152"
        return "sms:+%s?body=%s" % (phone, text)

    def sharelink(self):
        text = """Beli %s murah. 
Paling Enak, Paling Aman, Paling Sehat.
"""% self.name
        text = urllib2.quote(text)
        url = "share:?title=%s&url=https://organikku.id/produk/detail/%s"% (text, self.slug) 
        return url
    def __str__(self):
        return self.name
    
class Article(models.Model):
    title = models.CharField(max_length=100)
    slug = models.CharField(max_length=350, blank=True, null=True, unique=True)
    shortdesc = models.CharField(max_length=300, blank=True, null=True)
    photo = models.ImageField('Photo', upload_to=get_article_upload, blank=True, null=True)
    content = HTMLField('Content')
    
    def __str__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.title)
        super(Article, self).save(*args, **kwargs)
    
admin.site.register(Article)

class Galery(models.Model):
    shortdesc = models.CharField(max_length=300, blank=True, null=True)
    photo = models.ImageField('Photo', upload_to=get_article_upload, blank=True, null=True)
        
    def __str__(self):
        return self.shortdesc
    
admin.site.register(Galery)

class Cart(models.Model):
    user = models.ForeignKey(UserProfile, blank=True, null=True)
    token = models.CharField(max_length=50, blank=True, null=True)
    creation_date = models.DateTimeField(blank=True, null=True, verbose_name='creation date')
    checked_out = models.BooleanField(default=False, verbose_name='checked out')

    def __str__(self):
        return self.token

    def get_total(self):
        total = 0
        for i in self.item_set.all():
            total += i.unit_price
        return total

admin.site.register(Cart)

class Item(models.Model):
    cart = models.ForeignKey(Cart, verbose_name='cart')
    quantity = models.PositiveIntegerField(default=0)
    unit_price = models.DecimalField(blank=True, null=True, max_digits=18, decimal_places=2)
    product = models.ForeignKey(Product)

    def save(self, *args, **kwargs):
        super(Item, self).save(*args, **kwargs)

    def get_price(self):
        unit_price = self.product.price * Decimal(self.quantity)
        return unit_price

    def __str__(self):
        return self.product.name   
    #untuk menampilkan di database
admin.site.register(Item)

