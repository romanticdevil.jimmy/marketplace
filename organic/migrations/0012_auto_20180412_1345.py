# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0011_auto_20180412_1341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='listdelivery',
            name='receiver',
            field=models.OneToOneField(related_name='Receiver', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
