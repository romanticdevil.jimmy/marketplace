# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0018_auto_20180413_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='listdelivery',
            name='orderstatus',
            field=models.BooleanField(default=False, verbose_name=b'sending'),
        ),
        migrations.AlterField(
            model_name='listdelivery',
            name='statusdeliivery',
            field=models.BooleanField(default=False, verbose_name=b'on proses'),
        ),
        migrations.AlterField(
            model_name='listdelivery',
            name='statustransfer',
            field=models.BooleanField(default=False, verbose_name=b'trasfered'),
        ),
    ]
