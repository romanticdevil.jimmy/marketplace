# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import organic.models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0024_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='photo',
        ),
        migrations.AddField(
            model_name='product',
            name='bukalapak',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='category',
            field=models.ForeignKey(blank=True, to='organic.Category', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='like',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='product',
            name='photo1',
            field=models.ImageField(upload_to=organic.models.get_p1_upload, null=True, verbose_name=b'Photo', blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='photo2',
            field=models.ImageField(upload_to=organic.models.get_p2_upload, null=True, verbose_name=b'Photo', blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='photo3',
            field=models.ImageField(upload_to=organic.models.get_p3_upload, null=True, verbose_name=b'Photo', blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='pricemarket',
            field=models.DecimalField(null=True, max_digits=15, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='ratings',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='product',
            name='shortdesc',
            field=models.CharField(max_length=300, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='toped',
            field=models.CharField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='weight',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.TextField(help_text=b'Deskripsi produk organik blablabla', null=True, blank=True),
        ),
    ]
