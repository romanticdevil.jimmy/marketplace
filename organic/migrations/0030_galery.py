# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import organic.models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0029_article_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='Galery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shortdesc', models.CharField(max_length=300, null=True, blank=True)),
                ('photo', models.ImageField(upload_to=organic.models.get_article_upload, null=True, verbose_name=b'Photo', blank=True)),
            ],
        ),
    ]
