# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0017_listdelivery_nbank'),
    ]

    operations = [
        migrations.AlterField(
            model_name='listdelivery',
            name='nbank',
            field=models.ForeignKey(blank=True, to='organic.Bank', null=True),
        ),
    ]
