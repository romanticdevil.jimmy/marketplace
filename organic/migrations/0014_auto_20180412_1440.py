# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0013_listdelivery_fullname'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bank',
            name='Name_receiver',
        ),
        migrations.AlterField(
            model_name='listdelivery',
            name='receiver',
            field=models.OneToOneField(related_name='Receiver', to='organic.Cart'),
        ),
    ]
