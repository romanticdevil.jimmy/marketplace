# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View
from django.views.generic.base import TemplateView
from organic.models import Product, Cart, Item, Article, Galery
import datetime
from django.middleware import csrf


def get_or_create_csrf_token(request):
    token = request.META.get('CSRF_COOKIE', None)
    if token is None:
        token = csrf._get_new_csrf_key()
        request.META['CSRF_COOKIE'] = token
    request.META['CSRF_COOKIE_USED'] = True
    return token


class HomePageView(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        objects = Product.objects.all()
        context = dict(
            objects=objects
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))

    
class Contact(View):
    template_name = 'contact.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(
           self.template_name,
           context_instance=RequestContext(request))

class About(View):
    template_name = 'about.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(
           self.template_name,
           context_instance=RequestContext(request))

    
class Detail(View):
    template_name = 'detail.html'
    
    def get(self, request, *args, **kwargs):
        slug=kwargs["slug"]
        product = Product.objects.get(slug=slug)
        context = dict(
            object=product
        )
        return render_to_response(
            self.template_name,
            context,               
            context_instance=RequestContext(request))

class ArticleView(View):
    template_name = 'article.html'

    def get(self, request, *args, **kwargs):
        objects = Article.objects.all()
        context = dict(
            objects=objects
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))
    
class DetailArticle(View):
    template_name = 'article_detail.html'
    
    def get(self, request, *args, **kwargs):
        slug=kwargs["slug"]
        objects = Article.objects.get(slug=slug)
        context = dict(
            objects=objects
        )
        return render_to_response(
            self.template_name,
            context,               
            context_instance=RequestContext(request))
    
class GaleryView(View):
    template_name = 'galery.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(
           self.template_name,
           context_instance=RequestContext(request))
    
class CartView(View):
    template_name = "cart.html"
    def get(self, request, *args, **kwargs):
        
        if request.GET.get("dl"):
            cart = Item.objects.get(id=request.GET.get('dl')).delete()
            return HttpResponseRedirect("/cart/")
        
        getcsrf = get_or_create_csrf_token(request)
        cart = Cart.objects.get(token=getcsrf)
        context = dict(
               object=cart
        )
        return render_to_response(
               self.template_name,
               context,
               context_instance=RequestContext(request))
    
    def post(self, request, *args, **kwargs):
        item = Item.objects.get(id=request.POST.get("key"))
        item.quantity = int(request.POST.get("qty"))
        item.unit_price = item.get_price()
        item.save()
        return HttpResponse("success")
    